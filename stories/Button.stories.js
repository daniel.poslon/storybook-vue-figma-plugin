import MyButton from './Button.vue';

export default {
  title: 'Example/Button',
  component: MyButton,
  argTypes: {
    // backgroundColor: {
    //   control: 'color',
    //   defaultValue: "#ffffff"
    // },
    onClick: {},
    label: {
      type: {
        name: "string",
        required: true
      },
      name: "label",
      defaultValue: "Function"
    },
    iconOnly: {
      type: "boolean",
      defaultValue: false
    },
    icon: {
      control: {
        type: "select",
      },
      options: ["bars", "search", "user"],
      defaultValue: "bars"
    }
  },
};

const Template = (args) => ({
  components: {
    MyButton
  },
  setup() {
    return {
      args
    };
  },
  template: '<my-button v-bind="args" />',
});

export const Primary = Template.bind({});
Primary.args = {
  icon: "bars"
};


Primary.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/zJeRaor7kAXyEuLOGit8vS/Component-Library?node-id=3:123",
    embedHost: 'localhost'
  }
}