import {
  app
} from "@storybook/vue3";
import {
  library
} from "@fortawesome/fontawesome-svg-core";
import {
  faBars,
  faSearch,
  faUser
} from "@fortawesome/free-solid-svg-icons";
import {
  FontAwesomeIcon
} from "@fortawesome/vue-fontawesome";

export const parameters = {
  actions: {
    argTypesRegex: "^on[A-Z].*"
  },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}



library.add(faBars, faSearch, faUser);
app.component("font-awesome-icon", FontAwesomeIcon);